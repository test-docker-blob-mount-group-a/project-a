FROM redis:6.0.6-alpine
RUN apk add openssl
RUN openssl rand -out data -base64 100 # generate unique layer with 100 random bytes
